This repo contains Grinder scripts for testing performance of Jira. But currently support only view persona.

Prerequisites
====================
Before start the test on your local machine, you need to populate some test data on the server. We will pre-populate the test data on Bamboo automatically.

1. Create few users and update 'src/main/resources/test_scripts/jira_users.txt'. (username,password)
2. Create few projects and update 'src/main/resources/test_scripts/jira_projects.txt'. (Project name,id,key)
3. Create few issues and update 'src/main/resources/test_scripts/jira_issues.txt'. (key,id)

How to run
====================
1. BTF - mvn clean verify
2. Ondemand - mvn clean verify -P ondemand
	
Report
====================
target/classes/logs
