from HTTPClient import NVPair
from env import request, cacheRequest, ignoredRequest, extract, valueOrEmpty, valueOrDefault, postMultipart, extractAll
from java.util.regex import Pattern
from java.lang import String
from form import extractOptions, extractSelects, extractSubmit, extractTextAreas, extractTextfields

class Issue:
    
    def __init__(self, testIndex, issueKey, issueId):
        self.issueKey = issueKey
        self.issueId = issueId
        self.projectKey = String(issueKey).split("-")[0]
        cacheIndex = testIndex * 100 
        
        self.requests = {
            'view' : request(testIndex, 'HTTP-REQ : view issue'),
            'add_comment' : request(testIndex + 1, 'HTTP-REQ : add comment to issue'),
            'edit_init' : request(testIndex + 2, 'HTTP-REQ : edit issue dialog'),
            'edit_dropdowns' : request(testIndex + 3, 'HTTP-REQ : edit issue populate dropdowns'),
            'edit' : request(testIndex + 4, 'HTTP-REQ : edit issue'),
            'workflow_transition' : request(testIndex + 5, 'HTTP-REQ : workflow transition'),
            'workflow_completion' : request(testIndex + 6, 'HTTP-REQ : workflow completion'),
            'activity_stream' : request(testIndex + 7, 'HTTP_REQ : issue activity tab activity streams'),
            'view_cache' : cacheRequest(cacheIndex, 'CACHE : view issue'),
            'workflow_cache' : cacheRequest(cacheIndex+1, 'CACHE : workflow'),
            'edit_cache' : cacheRequest(cacheIndex+2, 'CACHE : edit issue'),
            'add_comment_cache' : cacheRequest(cacheIndex+3, 'CACHE : add comment'),

        }
        self.patterns = {
            'stream' : Pattern.compile('id="gadget-stream.*?src="http:\/\/.*?(\/.*?)"'),
            'tab_activity_stream' : Pattern.compile('id="gadget-0".*?src="http:\/\/.*?(\/.*?)"'),
            'project_avatar' : Pattern.compile('id="project-avatar".*?src="(.*?)"'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar?.*?)\)'),
            'edit_user_avatars' : Pattern.compile('(/secure/useravatar.*?)\''),
            'atl_token' : Pattern.compile('id="atlassian-token".*?content="(.*?)"'),
            'edit_summary' : Pattern.compile('(?s)id=\\\\"summary\\\\".*?value=\\\\"(.*?)\\\\"'),
            'edit_issuetype' : Pattern.compile('(?s)id=\\\\"issuetype\\\\".*?selected=\\\\"selected\\\\".*?value=\\\\"([0-9]*)\\\\"'),
            'edit_priority' : Pattern.compile('(?s)id=\\\\"priority\\\\".*?selected=\\\\"selected\\\\".*?value=\\\\"([0-9]*)\\\\"'),
            'edit_assignee' : Pattern.compile('(?s)id=\\\\"assignee\\\\".*?value=\\\\"(.*?)\\\\"'),
            'edit_description' : Pattern.compile('(?s)id=\\\\"description\\\\".*?\>(.*?)\<'),
            'edit_reporter' : Pattern.compile('(?s)id=\\\\"reporter\\\\".*?value=\\\\"(.*?)\\\\"'),
            'transition_assignee' : Pattern.compile('(?s)id="assignee".*?selected="selected".*?value="(.*?)"'),
            'workflow_action' : Pattern.compile('\/secure\/WorkflowUIDispatcher\.jspa.*?action=([0-9]*)'),
            'workflow_form' : Pattern.compile('(?s)form (action="\/secure\/CommentAssignIssue.*?)<\/form'),            
            'is_closed' : Pattern.compile('(?s)\/images\/icons\/status_closed.gif')               
        }


    def getKey(self):
        return self.issueKey
    

    def view(self, cached=False):
        req = self.requests['view']
        page = req.GET('/browse/' + self.issueKey).text
        cache_req = self.requests['view_cache']
        if not cached:
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/6f5136d896af3f682ae983a1ca4231d3/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cache_req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=issueaction&context=issuenavigation')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/button_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_blockexpanded.png')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/status_open.gif')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            
            cache_req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), ) ))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                cache_req.GET(userAvatar)
                
        return { 
            'workflow_actions' : extractAll(page, self.patterns['workflow_action']),
            'editable' : not self.patterns['is_closed'].matcher(page).find(),
            'atl_token' : extract(page, self.patterns['atl_token'])
        }


    def nextWorkflowStep(self, atl_token, stepId, values={}, cached=False):
        wftrans_req = self.requests['workflow_transition']
        wfcompletion_req = self.requests['workflow_completion']
        cache_req = self.requests['workflow_cache']
        ignored_req = ignoredRequest()
        if not cached:
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/includes/jquery/plugins/fancybox/fancybox.png')

        response = wftrans_req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=' + stepId + '&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328236631852')
        
        # workflow action without screen mutates on GET
        if response.getStatusCode() == 302:
            # extract all form data and construct POST request
            page = ignored_req.GET(response.getHeader('Location')).text
            
            form = extract(page, self.patterns['workflow_form'])
            params = []
            
            for select in extractSelects(form):
                name = select[0]
                if name != 'fixVersions':   #frother evasion
                    options = extractOptions(select[1])
                    params.append(NVPair(name, valueOrDefault(values, name, options[0])))
            
            for textfield in extractTextfields(form):
                params.append(NVPair(textfield, valueOrEmpty(values, textfield)))
                
            for textarea in extractTextAreas(form):
                if textarea != 'fixVersions': #more frother evasion
                    params.append(NVPair(textarea, valueOrEmpty(values, textarea)))
                
            params.append(NVPair(extractSubmit(form), ''))
            params.append(NVPair('action', stepId))
            params.append(NVPair('id', self.issueId))
            params.append(NVPair('atl_token', atl_token))
            
            wfcompletion_req.POST('/secure/CommentAssignIssue.jspa', params, ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ))
        
        return self.view(cached)
    

    def startProgress(self, atl_token, cached=False):
        req = self.requests['start_progress']
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/images/icons/status_inprogress.gif')
           
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=4&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328236631852')
        return self.view(cached)
        

    def stopProgress(self, atl_token, cached=False):
        req = self.requests['stop_progress']

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/images/icons/status_open.gif')
        
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=301&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328237770980')
        return self.view(cached)
        

    def resolve(self, atl_token, cached=False):
        req = self.requests['resolve']
  
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=5&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328238748158')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?action=5&inline=true&atl_token=' + atl_token + '&id=' + self.issueId + '&decorator=dialog&_=1328238748158').text

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/images/icons/aui-icon-forms.gif')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-4qg4om/732/5/1/_/includes/jquery/plugins/fancybox/fancybox.png')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '5'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('resolution', '1'),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), NVPair('X-Atlassian-Token', 'no-check')))

        return self.view(cached)


    def close(self, atl_token, cached=False):
        req = self.requests['close']
        
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=701&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328240402504')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?decorator=dialog&atl_token=' + atl_token + '&_=1328240402504&id=' + self.issueId + '&action=701&inline=true').text

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '701'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), NVPair('X-Atlassian-Token', 'no-check') ) )

        return self.view(cached)
        
        
    def reopen(self, atl_token, cached=False):
        req = self.requests['reopen']
                
        req.GET('/secure/WorkflowUIDispatcher.jspa?id=' + self.issueId + '&action=3&atl_token=' + atl_token + '&decorator=dialog&inline=true&_=1328240851124')
        page = req.GET('/secure/CommentAssignIssue!default.jspa?id=' + self.issueId + '&atl_token=' + atl_token + '&_=1328240851124&action=3&decorator=dialog&inline=true').text

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1/_/includes/jquery/plugins/fancybox/fancybox.png')    
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
        
        req.POST('/secure/CommentAssignIssue.jspa?atl_token=' + atl_token,
        (
            NVPair('action', '3'),
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('viewIssueKey', ''),
            NVPair('assignee', extract(page, self.patterns['transition_assignee'])),
            NVPair('comment', ''),
            NVPair('commentLevel', ''),
        ),
        ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), NVPair('X-Atlassian-Token', 'no-check') ) )
        
        return self.view(cached)
                
    def viewTabAll(self, cached=False):
        req = self.requests['view']
        cache_req = self.requests['view_cache']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel').text

        if not cached:        
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cache_req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=issueaction&context=issuenavigation')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/button_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_blockexpanded.png')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/status_open.gif')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
        
            cache_req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                cache_req.GET(userAvatar)
        
        
    def viewTabComments(self, cached=False):
        req = self.requests['view']
        cache_req = self.requests['view_cache']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel').text
        
        if not cached:
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cache_req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=issueaction&context=issuenavigation')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/button_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_blockexpanded.png')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/status_open.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            
            cache_req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))        

            
    def viewTabChangeHistory(self, cached=False):
        req = self.requests['view']
        cache_req = self.requests['view_cache']
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.jira.plugin.system.issuetabpanels:changehistory-tabpanel').text
        
        if not cached:
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cache_req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=issueaction&context=issuenavigation')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/button_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_blockexpanded.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/status_open.gif')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
        
            cache_req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            for userAvatar in extractAll(page, self.patterns['user_avatars'], (('&amp;', '&'), )):
                cache_req.GET(userAvatar)
        

    def viewTabActivityStream(self, cached=False):
        req = self.requests['view']
        cache_req = self.requests['view_cache']
        activity_req = self.requests['activity_stream']
        ignored_req = ignoredRequest()
        
        page = req.GET('/browse/' + self.issueKey + '?page=com.atlassian.streams.streams-jira-plugin:activity-stream-issue-tab').text
        ignored_req.GET(extract(page, self.patterns['tab_activity_stream']))
        ignored_req.GET('/rest/activity-stream/1.0/preferences')
        activity_req.GET('/plugins/servlet/streams?maxResults=20&streams=issue-key+IS+' + self.issueKey + '&streams=key+IS+' + self.projectKey)
        ignored_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328655669854')
        
        if not cached:
            cache_req.GET(extract(page, self.patterns['project_avatar'], (('&amp;', '&'), )))
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/css/jira.view.issue,atl.general/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cache_req.GET('/s/en_US-4qg4om/732/5/4d9d8c94d552e173e42c848006a83f12/_/download/contextbatch/js/jira.view.issue,atl.general/batch.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jquery-fancybox/jira.webresources:jquery-fancybox.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:viewissue/jira.webresources:viewissue.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cache_req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=issueaction&context=issuenavigation')
            cache_req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/button_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/mod_header_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/view_20.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_blockexpanded.png')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/images/icons/status_open.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cache_req.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            cache_req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.0-rc4/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            cache_req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            cache_req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            cache_req.GET('/s/en_USkppxta/712/13/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/menu_indicator_for_light_backgrounds.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets/images/tools_12.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')


    def addComment(self, atl_token, comment={ 'comment' : 'test' }, cached=False):
        req = self.requests['add_comment']
        cache_req = self.requests['add_comment_cache']
      
         
        if not cached:
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/stalker-ext-grad.png')
                
        response = req.POST('/secure/AddComment.jspa?',
        (
            NVPair('atl_token', atl_token),
            NVPair('id', self.issueId),
            NVPair('comment', valueOrEmpty(comment, 'comment')),
            NVPair('commentLevel', valueOrEmpty(comment, 'commentLevel')),
            NVPair('Add', 'Add'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        )).getText()
        extract(response, '')



    def edit(self, atl_token, edit={}, cached=False):
        edit_req = self.requests['edit']
        edit_init_req = self.requests['edit_init']
        edit_dropdowns_req = self.requests['edit_dropdowns']
        cache_req = self.requests['edit_cache']
        
        dialog = edit_init_req.POST('/secure/QuickEditIssue!default.jspa?issueId=' + self.issueId + '&decorator=none').text
                
        if not cached:        
            cache_req.GET('/images/icons/ico_help.png')
            cache_req.GET('/images/icons/filter_public.gif')
            cache_req.GET('/images/icons/newfeature.gif')
            cache_req.GET('/images/icons/task.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0.28/_/download/resources/com.atlassian.jira.jira-quick-edit-plugin:quick-form/images/icon-cog.png')
            cache_req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            cache_req.GET('/images/icons/improvement.gif')
            cache_req.GET('/images/icons/priority_blocker.gif')
            cache_req.GET('/images/icons/bug.gif')
            cache_req.GET('/images/icons/priority_critical.gif')
            cache_req.GET('/images/icons/priority_minor.gif')
            
        edit_dropdowns_req.POST('/rest/quickedit/1.0/userpreferences/edit',
                 '{\"useQuickForm\":false,\"fields\":[\"fixVersions\",\"assignee\",\"labels\",\"components\",\"priority\",\"comment\"],\"showWelcomeScreen\":true}',
                 ( NVPair('Content-Type', 'application/json'), NVPair('X-Atlassian-Token', 'no-check') )
            )

        if not cached:
            for userAvatar in extractAll(dialog, self.patterns['edit_user_avatars'], (('&amp;', '&'), )):
                cache_req.GET(userAvatar)
            cache_req.GET('/images/icons/priority_trivial.gif')
            cache_req.GET('/images/icons/priority_major.gif')
            cache_req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')

        params = [
            NVPair('id', self.issueId),
            NVPair('atl_token', atl_token),
#            NVPair('reporter', valueOrDefault(edit, 'reporter', extract(dialog, self.patterns['edit_reporter']))),
            NVPair('summary', valueOrDefault(edit, 'summary', extract(dialog, self.patterns['edit_summary']))),
            NVPair('issuetype', '2'),
            NVPair('reporter', 'admin'),
            NVPair('assignee', 'admin2'),
            NVPair('environment', ''),
            NVPair('description', ''),
            NVPair('duedate', ''),
            NVPair('comment', ''),
            NVPair('commentLevel', '')
        ]

            
        response = edit_req.POST('/secure/QuickEditIssue.jspa?issueId=' + self.issueId + '&decorator=none', params, ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), NVPair('X-Atlassian-Token', 'no-check'))).getText()
        extract(response, '')
        
        return self.view(cached)
