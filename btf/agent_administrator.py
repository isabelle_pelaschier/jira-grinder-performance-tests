from net.grinder.plugin.http import HTTPPluginControl
from net.grinder.script import Test
from HTTPClient import NVPair
from interactions import *

connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started

connectionDefaults.defaultHeaders = \
  [ NVPair('Accept-Language', 'en-US,en;q=0.8'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
    NVPair('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'),
    NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'), ]

USERS_CREATE = 10
PROJECTS_CREATE = 10
PROJECTS_EDIT = 10
ISSUETYPE_CREATE = 5
ISSUETYPE_EDIT = 5
WORKFLOW_CREATE = 5
WORKFLOW_EDIT = 5
SCREENS_CREATE = 5
SCREENS_EDIT = 5
NOTIFICATION_CREATE = 5
NOTIFICATION_EDIT = 5
STATUS_CREATE = 2
STATUS_EDIT = 1
PRIORITY_CREATE = 2
PRIORITY_EDIT = 1
RESOLUTION_CREATE = 2
RESOLUTION_EDIT = 2
PERMISSION_CREATE = 5
PERMISSION_EDIT = 5
SYSTEM_INFO = 2
LOGLEVEL_CHANGE = 3
MAILSERVER_ADD = 1
INTRODUCTION_CHANGE = 4

class TestRunner:
    
    def __call__(self):
        goHome()
        doLoginAsAdmin()
        doWebsudoLogin()
        createUser(USERS_CREATE)
        createProject(PROJECTS_CREATE)
        editProject(PROJECTS_EDIT)   
        createIssueType(ISSUETYPE_CREATE)
        editIssueType(ISSUETYPE_EDIT)
        createWorkflow(WORKFLOW_CREATE)
        editWorkflow(WORKFLOW_EDIT)
        createScreen(SCREENS_CREATE)
        editScreen(SCREENS_EDIT)
        createNotificationScheme(NOTIFICATION_CREATE)
        editNotificationScheme(NOTIFICATION_EDIT)
        createPriority(PRIORITY_CREATE)
        editPriority(PRIORITY_EDIT)
        createStatus(STATUS_CREATE)
        editStatus(STATUS_EDIT)
        createResolution(RESOLUTION_CREATE)
        editResolution(RESOLUTION_EDIT)
        createPermissionScheme(PERMISSION_CREATE)
        editPermissionScheme(PERMISSION_EDIT)
        browseSystemInfo(SYSTEM_INFO)
        changeLogLevel(LOGLEVEL_CHANGE)
        addMailServer(MAILSERVER_ADD)
        changeGeneralConfiguration(INTRODUCTION_CHANGE)

Test(1, 'dashboard - not logged in').wrap(goHome)
Test(2, 'login & open dashboard').wrap(doLoginAsAdmin)
Test(14, 'create ' + str(USERS_CREATE) + ' users').wrap(createUser)
Test(15, 'create ' + str(PROJECTS_CREATE) + ' projects').wrap(createProject)
Test(16, 'edit ' + str(PROJECTS_EDIT) + ' projects').wrap(editProject)
Test(17, 'create ' + str(ISSUETYPE_CREATE) + ' issue types').wrap(createIssueType)
Test(18, 'edit ' + str(ISSUETYPE_EDIT) + ' issue types').wrap(editIssueType)
Test(19, 'create ' + str(WORKFLOW_CREATE) + ' workflows').wrap(createWorkflow)
Test(20, 'edit ' + str(WORKFLOW_EDIT) + ' workflows').wrap(editWorkflow)
Test(21, 'create ' + str(SCREENS_CREATE) + ' screens').wrap(createScreen)
Test(22, 'edit ' + str(SCREENS_EDIT) + ' screen').wrap(editScreen)
Test(23, 'create ' + str(NOTIFICATION_CREATE)  + ' notification schemes').wrap(createNotificationScheme)
Test(24, 'edit ' + str(NOTIFICATION_EDIT) + ' notification schemes').wrap(editNotificationScheme)
Test(25, 'create ' + str(PRIORITY_CREATE) + ' priorities').wrap(createPriority)
Test(26, 'edit ' + str(PRIORITY_EDIT) + ' priorities').wrap(editPriority)
Test(27, 'create ' + str(STATUS_CREATE) + ' statuses').wrap(createStatus)
Test(28, 'edit ' + str(STATUS_EDIT) + ' statuses').wrap(editStatus)
Test(29, 'create ' + str(RESOLUTION_CREATE) + ' resolutions').wrap(createResolution)
Test(30, 'edit ' + str(RESOLUTION_EDIT) + ' resolutions').wrap(editResolution)
Test(31, 'create ' + str(PERMISSION_CREATE) + ' permission schemes').wrap(createPermissionScheme)
Test(32, 'edit ' + str(PERMISSION_EDIT) + ' permission schemes').wrap(editPermissionScheme)
Test(33, 'browse system info ' + str(SYSTEM_INFO) + ' times').wrap(browseSystemInfo)
Test(34, 'change log level ' + str(LOGLEVEL_CHANGE) + ' times').wrap(loggingProfilingAdmin)
Test(35, 'add ' + str(MAILSERVER_ADD) + ' smtp servers').wrap(addMailServer)
Test(36, 'change introduction ' + str(INTRODUCTION_CHANGE) + ' times').wrap(changeGeneralConfiguration)
Test(37, 'websudo login').wrap(doWebsudoLogin)
