from env import randint

class UserList:
    
    def __init__(self, config):
        self.users = []
        self.config = config
        
        rowCount = self.getRowCount(config.getUserFile())
        bounds = self.getBounds(rowCount, config.getTotalWorkers(), config.getCurrentWorker())
        print "######### row count %s" %rowCount
        
        f = open(config.getUserFile(), 'r')
        try:
            for i in range(rowCount):
                line = f.readline()
                #if i > bounds[1]:
                #    break
                
                #if i >= bounds[0]:
                print "######### %s" %line
                userData = line.split(',')
                name = userData[0]
                passwd = userData[1].replace('\n', '')
                if name == 'admin':
                    self.admin = (name, passwd)
                else:
                    self.users.append((name, passwd))
            
        finally:
            f.close()
            
    def getRandomUser(self):
        user = self.users[randint(0, len(self.users)-1)]
        return {
            'username' : user[0],
            'password' : user[1]
        }
    
    def getAdminUser(self):
        return {
            'username' : 'admin', #self.admin[0],
            'password' : 'admin' #self.admin[1]
        }
    
    def getBounds(self, rowCount, workerCount, workerIndex):
        usersPerWorker = rowCount / workerCount
        i = usersPerWorker * workerIndex
        return (i, i + usersPerWorker)
        
    def getRowCount(self, path):
        f = open(path, 'r')
        try:
            for i, l in enumerate(f):
                pass
            
            return i+1
        finally:
            f.close()
