from net.grinder.plugin.http import HTTPPluginControl
from net.grinder.script import Test
from HTTPClient import NVPair
from interactions import *

connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started

connectionDefaults.defaultHeaders = \
  [ NVPair('Accept-Language', 'en-US,en;q=0.8'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
    NVPair('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'),
    NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'), ]

ISSUE_COUNT_VIEW = 4
ISSUE_COUNT_VIEW_TABS = 4
ISSUE_COUNT_TRANSITION = 4
PROJECT_VIEWS = 4
ISSUE_SEARCHES = 4
FILTER_SEARCHES = 20
ISSUE_COUNT_EDIT = 20
ISSUE_COUNT_COMMENT = 20

class TestRunner:
    
    def __call__(self):
        goHome()
        doLogin()
        searchIssues(ISSUE_SEARCHES)
        browseIssues(ISSUE_COUNT_VIEW)
        browseIssueTabs(ISSUE_COUNT_VIEW_TABS)
        ## editIssues(ISSUE_COUNT_EDIT)
        #addComments(ISSUE_COUNT_COMMENT)
        #transitionIssues(ISSUE_COUNT_TRANSITION)
        #useSavedFilter(FILTER_SEARCHES)
        browseProjects(PROJECT_VIEWS)
        browseUserProfile()

Test(1, 'view dashboard - not logged in').wrap(goHome)
Test(2, 'login & view dashboard').wrap(doLogin)
Test(7, 'perform ' + str(ISSUE_SEARCHES) + ' searches in the issue navigator').wrap(searchIssues)
Test(3, 'view ' + str(ISSUE_COUNT_VIEW) + ' issues').wrap(browseIssues)
Test(37, 'view ' + str(ISSUE_COUNT_VIEW_TABS) + ' issues').wrap(browseIssueTabs)
# Test(4, 'edit ' + str(ISSUE_COUNT_EDIT) + ' issues').wrap(editIssues)
Test(5, 'add comment to ' + str(ISSUE_COUNT_COMMENT) + ' issues').wrap(addComments)
Test(6, 'do ' + str(ISSUE_COUNT_TRANSITION) + ' workflow transitions').wrap(transitionIssues)
Test(8, 'do ' + str(FILTER_SEARCHES) + ' searches via saved filters').wrap(useSavedFilter)
Test(9, 'view ' + str(PROJECT_VIEWS) + ' projects with random tabs').wrap(browseProjects)
Test(10, 'view user profile').wrap(browseUserProfile)
