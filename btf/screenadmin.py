from HTTPClient import NVPair 
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class ScreenAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view screens'),
            'add' : request(testId + 1, 'HTTP-REQ : add screen'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit screen')
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="fieldScreenName".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="fieldScreenDescription".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_token_dialog' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"')
        }
        
        
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewFieldScreens.jspa').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
        
        return extract(page, self.patterns['atl_token'])
    
            
    def add(self, screen, cached=False):
        req = self.requests['add']

        dialog = req.GET('/secure/admin/AddNewFieldScreen.jspa?inline=true&decorator=dialog&_=1329957062969').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')

        req.POST('/secure/admin/AddFieldScreen.jspa',
        (
            NVPair('atl_token', extract(dialog, self.patterns['atl_token_dialog'])),
            NVPair('fieldScreenName', screen['name']),
            NVPair('fieldScreenDescription', valueOrEmpty(screen, 'description')),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))

        return self.browse(cached)
    
    
    def edit(self, token, screen, cached=False):
        req = self.requests['edit']
        sid = screen['id']

        page = req.GET('/secure/admin/EditFieldScreen!default.jspa?id=' + sid).text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/required.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
                
        req.POST('/secure/admin/EditFieldScreen.jspa',
        (
            NVPair('atl_token', token),
            NVPair('fieldScreenName', valueOrDefault(screen, 'name', extract(page, self.patterns['edit_name']))),
            NVPair('fieldScreenDescription', valueOrDefault(screen, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('Update', 'Update'),
            NVPair('id', sid),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))

        return self.browse(cached)
