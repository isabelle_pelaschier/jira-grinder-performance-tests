from HTTPClient import NVPair 
from env import request, extract, valueOrDefault
from java.util.regex import Pattern

class GeneralConfigAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view general configuration'),
            'edit' : request(testId + 1, 'HTTP-REQ : edit general configuration')
        }
        self.patterns = {
            'edit_title' : Pattern.compile('(?s)name="title".*?value="(.*?)"'),
            'edit_mode' : Pattern.compile('(?s)id="mode_select".*value="(.*?)" SELECTED'),
            'edit_captcha' : Pattern.compile('value="(.*?)" name="captcha".*?checked'),
            'edit_baseURL' : Pattern.compile('(?s)name="baseURL".*?value="(.*?)"'),
            'edit_emailFromHeaderFormat' : Pattern.compile('(?s)name="emailFromHeaderFormat".*?value="(.*?)"'),
            'edit_introduction' : Pattern.compile('(?s)name="introduction".*?\>(.*?)\<'),
            'edit_language' : Pattern.compile('(?s)name="language".*?value="([a-z]*?)" SELECTED'),
            'edit_voting' : Pattern.compile('value="(.*?)" name="voting".*?checked'),
            'edit_watching' : Pattern.compile('value="(.*?)" name="watching".*?checked'),
            'edit_allowUnassigned' : Pattern.compile('value="(.*?)" name="allowUnassigned".*?checked'),
            'edit_externalUM' : Pattern.compile('value="(.*?)" name="externalUM".*?checked'),
            'edit_logoutConfirm' : Pattern.compile('value="(.*?)".*?name="logoutConfirm" checked'),
            'edit_useGzip' : Pattern.compile('value="(.*?)" name="useGzip".*?checked'),
            'edit_allowRpc' : Pattern.compile('value="(.*?)" name="allowRpc".*?checked'),
            'edit_emailVisibility' : Pattern.compile('value="([a-z]*?)".*?name="emailVisibility" checked'),
            'edit_groupVisibility' : Pattern.compile('value="([a-z]*?)".*?name="groupVisibility" checked'),
            'edit_excludePrecedenceHeader' : Pattern.compile('value="(.*?)" name="excludePrecedenceHeader".*?checked'),
            'edit_ajaxIssuePicker' : Pattern.compile('value="(.*?)" name="ajaxIssuePicker".*?checked'),
            'edit_ajaxUserPicker' : Pattern.compile('value="(.*?)" name="ajaxUserPicker".*?checked'),
            'edit_jqlAutocompleteDisabled' : Pattern.compile('value="(.*?)" name="jqlAutocompleteDisabled".*?checked'),
            'edit_showContactAdministratorsForm' : Pattern.compile('value="(.*?)" name="showContactAdministratorsForm".*?checked'),
            'edit_ieMimeSniffer' : Pattern.compile('(?s)name="ieMimeSniffer".*?value="([a-z]*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']

        req.GET('/secure/admin/ViewApplicationProperties.jspa')

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            
        
    def edit(self, config, cached=False):
        req = self.requests['edit']
        
        page = req.GET('/secure/admin/EditApplicationProperties!default.jspa').text

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/47920e61982c493f855daea658d0bd34/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js')
            req.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_preview.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
                
        token = extract(page, self.patterns['atl_token'])
                
        req.POST('/secure/admin/EditApplicationProperties.jspa',
            (
                NVPair('atl_token', token),
                NVPair('maximumAuthenticationAttemptsAllowed', '3'),
                NVPair('title', valueOrDefault(config, 'title', extract(page, self.patterns['edit_title']))),
                NVPair('mode', valueOrDefault(config, 'mode', extract(page, self.patterns['edit_mode']))),
                NVPair('captcha', valueOrDefault(config, 'captcha', extract(page, self.patterns['edit_captcha']))),
                NVPair('baseURL', valueOrDefault(config, 'baseURL', extract(page, self.patterns['edit_baseURL']))),
                NVPair('emailFromHeaderFormat', valueOrDefault(config, 'emailFromHeaderFormat', extract(page, self.patterns['edit_emailFromHeaderFormat']))),
                NVPair('introduction', valueOrDefault(config, 'introduction', extract(page, self.patterns['edit_introduction']))),
                NVPair('language', valueOrDefault(config, 'language', extract(page, self.patterns['edit_language']))),
                NVPair('defaultLocale', '-1'),
                NVPair('timeZoneRegion', 'System'),
                NVPair('defaultTimeZoneId', 'System'),
                NVPair('voting', valueOrDefault(config, 'voting', extract(page, self.patterns['edit_voting']))),
                NVPair('watching', valueOrDefault(config, 'watching', extract(page, self.patterns['edit_watching']))),
                #NVPair('allowUnassigned', valueOrDefault(config, 'allowUnassigned', extract(page, self.patterns['edit_allowUnassigned']))),
                NVPair('externalUM', valueOrDefault(config, 'externalUM', extract(page, self.patterns['edit_externalUM']))),
                NVPair('logoutConfirm', valueOrDefault(config, 'logoutConfirm', extract(page, self.patterns['edit_logoutConfirm']))),
                NVPair('useGzip', valueOrDefault(config, 'useGzip', extract(page, self.patterns['edit_useGzip']))),
                NVPair('allowRpc', valueOrDefault(config, 'allowRpc', extract(page, self.patterns['edit_allowRpc']))),
                NVPair('emailVisibility', valueOrDefault(config, 'emailVisibility', extract(page, self.patterns['edit_emailVisibility']))),
                NVPair('groupVisibility', valueOrDefault(config, 'groupVisibility', extract(page, self.patterns['edit_groupVisibility']))),
                NVPair('excludePrecedenceHeader', valueOrDefault(config, 'excludePrecedenceHeader', extract(page, self.patterns['edit_excludePrecedenceHeader']))),
                NVPair('ajaxIssuePicker', valueOrDefault(config, 'ajaxIssuePicker', extract(page, self.patterns['edit_ajaxIssuePicker']))),
                NVPair('jqlAutocompleteDisabled', valueOrDefault(config, 'jqlAutocompleteDisabled', extract(page, self.patterns['edit_jqlAutocompleteDisabled']))),
                NVPair('ieMimeSniffer', valueOrDefault(config, 'ieMimeSniffer', extract(page, self.patterns['edit_ieMimeSniffer']))),
                NVPair('showContactAdministratorsForm', valueOrDefault(config, 'showContactAdministratorsForm', extract(page, self.patterns['edit_showContactAdministratorsForm']))),
                NVPair('contactAdministratorsMessage', ''),
                NVPair('Update', 'Update'),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            ))
        
        self.browse(cached)
