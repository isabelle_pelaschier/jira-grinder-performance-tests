from env import request, cacheRequest, ignoredRequest, extract
from java.util.regex import Pattern

class Reports:
    
    def __init__(self, testId):
      	cacheIndex = testId * 100
        self.requests = {
            'configure_init' : request(testId, 'HTTP REQ : initialise configure report'),
            'configure_picker' : request(testId + 1, 'HTTP REQ : report project picker'),
            'configure' : request(testId + 2, 'HTTP REQ : configure report'),

            'report_init' : request(testId + 3, 'HTTP REQ : initialise view report'),
            'report' : request(testId + 4, 'HTTP REQ : view report'),
            'report_cache' : cacheRequest(cacheIndex, 'CACHE : reports'),
            'freechart'    : ignoredRequest()
        }
        self.patterns = {
            'chart' : Pattern.compile('(\/charts\?filename=jfreechart-onetime-.*?\.png)')
        }
        
    def viewCreatedVsResolved(self, projectId, cached=False):
        initReq = self.requests['configure_init']
        pickerReq = self.requests['configure_picker']
        req = self.requests['configure']
        cacheReq = self.requests['report_cache']
        chartReq = self.requests['freechart']
               
        initReq.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report')
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')

        pickerReq.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
                
        pickerReq.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
                
        reportReq = self.requests['report']
        page = reportReq.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&cumulative=true&versionLabels=major&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:createdvsresolved-report&Next=Next').text

        if self.patterns['chart'].matcher(page).find():
            chartReq.GET(extract(page, self.patterns['chart']))

        
    def viewRecentlyCreatedIssues(self, projectId, cached=False):
        initReq = self.requests['configure_init']
        pickerReq = self.requests['configure_picker']
        req = self.requests['report']
        cacheReq = self.requests['report_cache']
        chartReq = self.requests['freechart']
        
        initReq.GET('/secure/ConfigureReport!default.jspa?selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report')
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/170468e8784374b9c178ab2bfc9a3fe7/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/712/378e85a786f108d2407c8cf432595238/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
                
        pickerReq.GET('/secure/FilterPickerPopup.jspa?showProjects=true&field=projectOrFilterId')
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
                
        pickerReq.GET('/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=projectOrFilterId')
        
        reportReq= self.requests['report']        
        page = reportReq.GET('/secure/ConfigureReport.jspa?projectOrFilterId=project-' + projectId + '&periodName=daily&daysprevious=30&selectedProjectId=' + projectId + '&reportKey=com.atlassian.jira.plugin.system.reports:recentlycreated-report&Next=Next').text
        if self.patterns['chart'].matcher(page).find():
            chartReq.GET(extract(page, self.patterns['chart']))
