from HTTPClient import NVPair
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class StatusAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view statuses'),
            'add' : request(testId + 1, 'HTTP-REQ : add status'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit status')
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'edit_iconurl' : Pattern.compile('(?s)name="iconurl".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewStatuses.jspa').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/images/icons/status_resolved.gif')
            req.GET('/images/icons/status_reopened.gif')
            req.GET('/images/icons/status_open.gif')
            req.GET('/images/icons/status_inprogress.gif')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/images/icons/status_generic.gif')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/required.png')
            req.GET('/images/icons/status_closed.gif')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')

        return extract(page, self.patterns['atl_token'])
            
        
    def add(self, token, status, cached=False):
        
        self.requests['add'].POST('/secure/admin/AddStatus.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', status['name']),
            NVPair('description', valueOrEmpty(status, 'description')),
            NVPair('iconurl', valueOrDefault(status, 'iconurl', '/images/icons/status_generic.gif')),
            NVPair('Add', 'Add'),
        ), (
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        return self.browse(cached)
                                  
                                  
    def edit(self, status, cached=False):
        req = self.requests['edit']
        sid = status['id']
        
        page = req.GET('/secure/admin/EditStatus!default.jspa?id=' + sid).text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
        
        token = extract(page, self.patterns['atl_token'])
        
        req.POST('/secure/admin/EditStatus.jspa',
            (
                NVPair('atl_token', token),
                NVPair('name', valueOrDefault(status, 'name', extract(page, self.patterns['edit_name']))),
                NVPair('description', valueOrDefault(status, 'description', extract(page, self.patterns['edit_description']))),
                NVPair('iconurl', valueOrDefault(status, 'iconurl', extract(page, self.patterns['edit_iconurl']))),
                NVPair('Update', 'Update'),
                NVPair('id', sid),
            ), ( 
                NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
                NVPair('X-Atlassian-Token', 'no-check') 
            ))

        self.browse(cached)
