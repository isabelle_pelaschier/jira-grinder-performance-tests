from HTTPClient import NVPair
from env import request

class WebSudo:
    
    def __init__(self, testId):
        self.request = request(testId, 'HTTP-REQ : websudo')
        
    def sudo(self, token, destination, password):
        self.request.POST('/secure/admin/WebSudoAuthenticate.jspa',
        (
            NVPair('webSudoPassword', password),
            NVPair('authenticate', 'Confirm'),
            NVPair('atl_token', token),
            NVPair('webSudoDestination', destination),
            NVPair('webSudoIsPost', 'false'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        ))          
