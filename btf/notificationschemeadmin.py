from HTTPClient import NVPair 
from env import request, extract, valueOrDefault, valueOrEmpty
from java.util.regex import Pattern

class NotificationSchemeAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : view notification schemes'),
            'add' : request(testId + 1, 'HTTP-REQ : add notification scheme'),
            'edit' : request(testId + 2, 'HTTP-REQ : edit notification scheme')
        }
        self.patterns = {
            'new_scheme_id' : Pattern.compile('schemeId=([0-9]*)'),
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'), 
            'edit_description' : Pattern.compile('(?s)name="description".*?\>*(.*?)\<'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }

    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/ViewNotificationSchemes.jspa').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')

        return extract(page, self.patterns['atl_token'])
        
        
    def add(self, token, scheme, cached=False):
        req = self.requests['add']
        
        page = req.GET('/secure/admin/AddNotificationScheme!default.jspa?atl_token=' + token).text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
                
        token = extract(page, self.patterns['atl_token'])
            
        response = req.POST('/secure/admin/AddNotificationScheme.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', scheme['name']),
            NVPair('description', valueOrEmpty(scheme, 'description')),
            NVPair('Add', 'Add'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
        ))
        
        newSchemeId = extract(response.getHeader('Location'), self.patterns['new_scheme_id'])
                 
        req.GET('/secure/admin/EditNotifications!default.jspa?schemeId=' + newSchemeId)
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')

        return newSchemeId
        
        
    def edit(self, token, scheme, cached=False):
        req = self.requests['edit']
        sid = scheme['id']
        
        page = req.GET('/secure/admin/EditNotificationScheme!default.jspa?atl_token=' + token + '&schemeId=' + sid).text

        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
                
        token = extract(page, self.patterns['atl_token'])
            
        req.POST('/secure/admin/EditNotificationScheme.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', valueOrDefault(scheme, 'name', extract(page, self.patterns['edit_name']))),
            NVPair('description', valueOrDefault(scheme, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('Update', 'Update'),
            NVPair('schemeId', sid),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        ))

        self.browse(cached)
