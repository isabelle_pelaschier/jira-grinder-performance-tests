from HTTPClient import NVPair
from env import request, valueOrEmpty
from env import valueOrDefault, extract
from java.util.regex import Pattern

class IssueTypeAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : manage issue types'), 
            'add' : request(testId + 1, 'HTTP-REQ : add issue type'), 
            'edit' : request(testId + 2, 'HTTP-REQ : edit issue type') 
        }
        self.patterns = {
            'edit_name' : Pattern.compile('(?s)name="name".*?value="(.*?)"'),
            'edit_description' : Pattern.compile('(?s)name="description".*?value="(.*?)"'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_token_dialog' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"')
        }
        
        
    def browse(self, cached=False):
        req = self.requests['browse']
        
        req.GET('/secure/admin/ViewIssueTypes.jspa')
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/tab_bg.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/images/icons/genericissue.gif')
            req.GET('/images/icons/improvement.gif')
            req.GET('/images/icons/newfeature.gif')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/images/icons/task.gif')
            req.GET('/images/icons/bug.gif')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')

    
    def add(self, issueType, cached=False):
        req = self.requests['add']

        dialog = req.GET('/secure/admin/AddNewIssueType.jspa?inline=true&decorator=dialog&_=1329951365493').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
                
        token = extract(dialog, self.patterns['atl_token_dialog'])

        req.POST('/secure/admin/AddIssueType.jspa',
        (
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
            NVPair('name', issueType['name']),
            NVPair('description', valueOrEmpty(issueType, 'description')),
            NVPair('iconurl', valueOrDefault(issueType, 'iconurl', '/images/icons/genericissue.gif')), 
            NVPair('atl_token', token),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'),
            NVPair('X-Atlassian-Token', 'no-check')  
        ))
            
        self.browse(cached)
                        

    def edit(self, issueType, cached=False):
        req = self.requests['edit']
        tid = issueType['id']
        
        page = req.GET('/secure/admin/EditIssueType!default.jspa?id=' + tid).text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/required.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')

        token = extract(page, self.patterns['atl_token'])

        req.POST('/secure/admin/EditIssueType.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', valueOrDefault(issueType, 'name', extract(page, self.patterns['edit_name']))),
            NVPair('description', valueOrDefault(issueType, 'description', extract(page, self.patterns['edit_description']))),
            NVPair('iconurl', '/images/icons/genericissue.gif'),
            NVPair('id', tid),
            NVPair('Update', 'Update'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        ))
        
        self.browse(cached)
