from HTTPClient import NVPair 
from env import request, valueOrDefault, valueOrEmpty, extract
from java.util.regex import Pattern

class MailServerAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : viewing mail servers'),
            'add' : request(testId + 1, 'HTTP-REQ : adding an SMTP server'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        self.patterns = {
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"')
        }
        
    
    def browse(self, cached=False):
        req = self.requests['browse']

        req.GET('/secure/admin/OutgoingMailServers.jspa')
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-mail-plugin:verifymailserverconnection/com.atlassian.jira.jira-mail-plugin:verifymailserverconnection.js')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')


    def addSmtpServer(self, server, cached=False):
        req = self.requests['add']
        
        page = req.GET('/secure/admin/AddSmtpMailServer!default.jspa').text        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-active-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:autocomplete/jira.webresources:autocomplete.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajax-favourite-control/jira.webresources:ajax-favourite-control.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:ajaxhistory/jira.webresources:ajaxhistory.js')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-mail-plugin:mail-servers/com.atlassian.jira.jira-mail-plugin:mail-servers.js')
            req.GET('/s/en_US-4qg4om/732/5/5.0.1-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-mail-plugin:verifymailserverconnection/com.atlassian.jira.jira-mail-plugin:verifymailserverconnection.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:share-types/jira.webresources:share-types.js')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:manageshared/jira.webresources:manageshared.js')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/required.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/icons/ico_help.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
                
        req.GET('/secure/admin/OutgoingMailServers.jspa')
                    
        token = extract(page, self.patterns['atl_token'])
                    
        req.POST('/secure/admin/AddSmtpMailServer.jspa',
        (
            NVPair('atl_token', token),
            NVPair('name', server['name']),
            NVPair('description', valueOrEmpty(server, 'description')),
            NVPair('from', server['from']),
            NVPair('serviceProvider', 'custom'),
            NVPair('prefix', valueOrDefault(server, 'prefix', 'test')),
            NVPair('serverName', server['servername']),
            NVPair('protocol', 'smtp'),
            NVPair('port', ''),
            NVPair('timeout', '10000'),
            NVPair('username', ''),
            NVPair('password', ''),
            NVPair('changePassword', 'true'),
            NVPair('jndiLocation', ''),
            NVPair('Add', 'Add'),
            NVPair('type', 'smtp'),
        ), (
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        ))
        
        self.browse(cached)
