from HTTPClient import NVPair
from env import request, cacheRequest, extract, valueOrEmpty, valueOrDefault, extractAll, randChoice, pause, resume
from java.util.regex import Pattern
from net.grinder.script.Grinder import grinder

class IssueCreator:
    
    def __init__(self, testIndex):
        cacheIndex = testIndex * 100
        self.requests = {
            
            'create_init' : request(testIndex, 'HTTP-REQ : create issue dialog'),
            'create_dropdowns' : request(testIndex + 1, 'HTTP-REQ : create issue populate dropdowns'),
            'create' : request(testIndex + 2, 'HTTP-REQ : create issue'),
            'create_cache' : cacheRequest(cacheIndex, 'CACHE : create issues'),
        }
        self.patterns = {
            'project_avatars' : Pattern.compile('background-image:url\((\/secure\/projectavatar.*?)\)'),
            'user_avatars' : Pattern.compile('background-image:url\((\/secure\/useravatar.*?)\)'),
            'create_response_issuekey' : Pattern.compile('"issueKey":"(.*?)"'),
            'atl_token' : Pattern.compile('"atl_token":"(.*?)"'),
            'options' : Pattern.compile('<option.*?value=\\\\"(.*?)\\\\"'),
        }
       
    def create(self, projectId, create={}, cached=False):
        initReq = self.requests['create_init']
        dropdownsReq = self.requests['create_dropdowns']
        req = self.requests['create']
        cacheReq = self.requests['create_cache']

        dialog = initReq.POST('/secure/QuickCreateIssue!default.jspa?decorator=none', ( NVPair('Content-Type', 'application/json'), ) ).text
        
        if not cached:
            cacheReq.GET('/images/icons/filter_public.gif')
            cacheReq.GET('/images/icons/ico_help.png')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/newfeature.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0.28/_/download/resources/com.atlassian.jira.jira-quick-edit-plugin:quick-form/images/icon-cog.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
            cacheReq.GET('/images/icons/improvement.gif')
            cacheReq.GET('/images/icons/task.gif')

            for userAvatar in extractAll(dialog, self.patterns['user_avatars'], (('&amp;', '&'), )):
                cacheReq.GET(userAvatar)
            for projectAvatar in extractAll(dialog, self.patterns['project_avatars'], (('&amp;', '&'), )):
                cacheReq.GET(projectAvatar)
            cacheReq.GET('/images/icons/priority_blocker.gif')
            cacheReq.GET('/images/icons/priority_minor.gif')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/images/icons/priority_critical.gif')
            cacheReq.GET('/images/icons/priority_trivial.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            
        dropdownsReq.POST('/rest/quickedit/1.0/userpreferences/create', 
                 '{\"useQuickForm\":false,\"fields\":[\"summary\",\"description\",\"priority\",\"versions\",\"components\"],\"showWelcomeScreen\":true}', 
                 ( NVPair('Content-Type', 'application/json'), )
        )
        #pause()
        atl_token = extract(dialog, self.patterns['atl_token'])
        def_issuetype = '3'
        #resume()
        response = req.POST('/secure/QuickCreateIssue.jspa?decorator=none',
        (
            NVPair('pid', projectId),
            NVPair('atl_token', 'B6CI-A2U1-7B37-1N9D|694bec01601a9f90cddffbd4fcaa3a3bf0b5eac4|lin'),
            NVPair('issuetype', valueOrDefault(create, 'issuetype', def_issuetype)),
            NVPair('summary', valueOrEmpty(create, 'summary')),
            NVPair('priority', valueOrDefault(create, 'priority', '3')),
            NVPair('assignee', valueOrDefault(create, 'assignee', '-1')),
            NVPair('reporter', create['reporter']),
            NVPair('description', valueOrEmpty(create, 'description')),
            NVPair('environment', valueOrEmpty(create, 'environment')),
            NVPair('duedate', ''),
            NVPair('fieldsToRetain', 'project'),
            NVPair('fieldsToRetain', 'issuetype'),
            NVPair('fieldsToRetain', 'priority'),
            NVPair('fieldsToRetain', 'duedate'),
            NVPair('fieldsToRetain', 'components'),
            NVPair('fieldsToRetain', 'versions'),
            NVPair('fieldsToRetain', 'fixVersions'),
            NVPair('fieldsToRetain', 'assignee'),
            NVPair('fieldsToRetain', 'reporter'),
            NVPair('fieldsToRetain', 'environment'),
            NVPair('fieldsToRetain', 'description'),
            NVPair('fieldsToRetain', 'labels'),
        ), ( 
            NVPair('contentType', 'application/json'),
            NVPair('X-Atlassian-Token', 'no-check') 
        )).getText()
        
        return #extractAll(response, self.patterns['create_response_issuekey'])
