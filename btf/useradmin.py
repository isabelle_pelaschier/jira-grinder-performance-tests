from HTTPClient import NVPair
from env import request, extractAll, extract
from java.util.regex import Pattern

class UserAdmin:
    
    def __init__(self, testId):
        self.requests = {
            'browse' : request(testId, 'HTTP-REQ : user browser'),
            'add' : request(testId + 1, 'HTTP-REQ : add user'), 
            'view' : request(testId + 2, 'HTTP-REQ : view user') 
        }
        self.patterns = {
            'user_avatars' : Pattern.compile('(?s)(\/secure\/useravatar\?.*?)\)'),
            'atl_token' : Pattern.compile('name="atlassian-token" content="(.*?)"'),
            'atl_dialog_token' : Pattern.compile('(?s)name="atl_token".*?value="(.*?)"')
        }
        
        
    def browseSudo(self):
        req = self.requests['browse']
        page = req.GET('/secure/admin/user/UserBrowser.jspa').text
        token = extract(page, self.patterns['atl_token'])
        return {
            'token' : token,
            'destination' : '/secure/admin/user/UserBrowser.jspa'
        }
       
        
    def browse(self, cached=False):
        req = self.requests['browse']
        
        page = req.GET('/secure/admin/user/UserBrowser.jspa').text
        
        if not cached:
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js')
            req.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            req.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js?cache=false')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/js/atl.admin/batch.js')
            req.GET('/rest/api/1.0/shortcuts/713/378e85a786f108d2407c8cf432595238/shortcuts.js?context=admin')
            req.GET('/s/en_US-4qg4om/732/5/40807407399fe70b65f6721c21729546/_/download/contextbatch/css/atl.admin/batch.css')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs/images/toolbar/aui-toolbar-button-inactive-bg.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
        
            for avatar in extractAll(page, self.patterns['user_avatars'], ( ('&amp;', '&'), )):
                req.GET(avatar)
        
            
    def add(self, user={}, cached=False):
        req = self.requests['add']

        dialog = req.GET('/secure/admin/user/AddUser!default.jspa?inline=true&decorator=dialog&_=1329897017260').text

        if not cached:        
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/includes/jquery/plugins/fancybox/fancybox.png')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-message-icon-sprite.png')
            req.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/wait.gif')
            req.GET('/secure/admin/user/AddUser!default.jspa?inline=true&decorator=dialog&_=1329897017260')
            req.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-forms.gif')
        
        username = user['username']
        password = user['password']

        req.POST('/secure/admin/user/AddUser.jspa',
        (
            NVPair('atl_token', extract(dialog, self.patterns['atl_dialog_token'])),
            NVPair('username', username),
            NVPair('password', password),
            NVPair('confirm', password),
            NVPair('fullname', username),
            NVPair('email', username + '@example.com'),
            NVPair('inline', 'true'),
            NVPair('decorator', 'dialog'),
        ), ( 
            NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
            NVPair('X-Atlassian-Token', 'no-check') 
        ))
        
        self.browse(cached)
