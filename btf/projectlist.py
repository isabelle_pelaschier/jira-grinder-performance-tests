from project import Project
from env import randint

class ProjectList:
    
    def __init__(self, baseTestIndex, projectFile):
        self.projects = []
        f = open(projectFile, 'r')
        try:
            lines = f.readlines()
            for i in range(len(lines)):
                projectData = lines[i].split(',')
                #print "=========%s" %projectData[0]
                print "=========%s" %projectData[1]
                print "=========%s" %projectData[2]
                self.projects.append(Project(baseTestIndex, projectData[2].replace('\n', ''), projectData[1]))
        finally:
            f.close()
            
    def getProjects(self):
        return self.projects
    
    def getRandomProject(self):
        return self.projects[randint(0, len(self.projects)-1)]
        
