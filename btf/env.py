from net.grinder.plugin.http import HTTPRequest, HTTPPlugin, HTTPPluginControl
from HTTPClient import NVPair, Codecs
from net.grinder.script import Test
from java.lang import System, String
from net.grinder.script.Grinder import grinder
from jarray import zeros
from java.util import Random

env = {
    'host' : System.getProperty("agent.jira.host")
}

if not env['host']:
    raise "host missing, please use -Dagent.jira.host=..."

def host():
    return env['host']

class Set:
    def __init__(self):
        self.values = []
    
    def add(self, val):
        if not self.values.__contains__(val):
            self.values.append(val)
        
    def seq(self):
        return self.values

def request(testId, label):
    req = HTTPRequest(url=host())
    if 'grinder.jira.http.record' in grinder.properties and grinder.properties['grinder.jira.http.record'] == 'false':
        return req
    else:
        return Test(testId, label).wrap(req)

def cacheRequest(testId, label):
    req = HTTPRequest(url=host())
    if 'grinder.jira.cache.record' in grinder.properties and grinder.properties['grinder.jira.cache.record'] == 'false':
        return req
    else:
        return Test(testId, label).wrap(req)

def ignoredRequest():
    return HTTPRequest(url=host())


def postMultipart(req, url, params):
    headers = zeros(1, NVPair)
    return req.POST(url, Codecs.mpFormDataEncode(params, (), headers), headers)

def extract(text, pattern, replace=()):
    grinder.logger.info(text)
    return

def log(text):
    grinder.logger.info(text)
    return
       
def extractAll(text, pattern, replace=()):
    matcher = pattern.matcher(text)
    values = Set()
    while matcher.find():
        val = matcher.group(1)
        for repl in replace:
            val = String(val).replace(repl[0], repl[1])
        values.add(val)
        
    return values.seq()

def valueOrEmpty(dataMap, key):
    return valueOrDefault(dataMap, key, '')

def valueOrDefault(dataMap, key, default):
    if key in dataMap:
        return dataMap[key]
    else:
        return default

rnd = Random()
def randint(lower, upper):
    grinder.logger.info(">>>>>>Upper %s"%upper)
    grinder.logger.info("Lower %s"%lower)
    if upper == lower:
        return upper
    
    #return rnd.nextInt(upper) + 1
    return 1

def randChoice(elems):
    return elems[randint(0, len(elems)-1)]

def pause():
    context=HTTPPluginControl.getThreadHTTPClientContext().getThreadContext();
    context.pauseClock()

def resume():
    context=HTTPPluginControl.getThreadHTTPClientContext().getThreadContext();
    context.resumeClock()
