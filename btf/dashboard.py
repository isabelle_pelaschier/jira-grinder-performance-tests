from env import request, cacheRequest, ignoredRequest, extract, host
from HTTPClient import NVPair
from java.util.regex import Pattern

class Dashboard:
    
    def __init__(self, testIndex):
        cacheIndex = testIndex * 100
        self.requests = {
            'home_not_logged_in' : request(testIndex, 'HTTP-REQ : home not logged in'),
            'login' : request(testIndex + 1, 'HTTP-REQ : perform login'),
            'home_logged_in' : request(testIndex + 2, 'HTTP-REQ : home logged in'),
            'activity_stream' : request(testIndex + 3, 'HTTP_REQ : activity streams'),
            'gadgets' : request(testIndex + 4, 'HTTP_REQ : gadgets'),
            'dashboard_cache' : cacheRequest(cacheIndex, 'CACHE : dashboard'),
            'ignored_req' : ignoredRequest()
        }
        self.gadgetRegex = {
            'login' : Pattern.compile('id":"0.*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'intro' : Pattern.compile('title":"Introduction".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'assignedToMe' : Pattern.compile('title":"Assigned\sto\sMe".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'favFilters' : Pattern.compile('title":"Favourite\sFilters".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"'),
            'admin' : Pattern.compile('title":"Admin".*?renderedGadgetUrl":"http:\\\/\\\/.*?(\\\/.*?)"')
        }
        
    # go to JIRA home and log in
    def login(self, username, password, cached=False):
        self.requests['login'].POST('/rest/gadget/1.0/login', ( 
                                        NVPair('os_username', username), 
                                        NVPair('os_password', password), 
                                        NVPair('os_captcha', ''), 
                                    ), ( 
                                        NVPair('Content-Type', 'application/x-www-form-urlencoded'), 
                                    )) 
        
            
    def loggedIn(self, cached=False):
        req = self.requests['home_logged_in']
        ignoredReq = self.requests['ignored_req']
        activityReq=self.requests['activity_stream']
        gadgetsReq=self.requests['gadgets']
        cacheReq = self.requests['dashboard_cache']
        ignoredReq.GET('/')
        ignoredReq.GET('/secure/MyJiraHome.jspa')
        dashPage = req.GET('/secure/Dashboard.jspa').text
        if self.gadgetRegex['intro'].matcher(dashPage).find():
            gadgetsReq.GET(extract(dashPage, self.gadgetRegex['intro'], (('\/', '/'), )))
        if self.gadgetRegex['assignedToMe'].matcher(dashPage).find():
            gadgetsReq.GET(extract(dashPage, self.gadgetRegex['assignedToMe'], (('\/', '/'), )))
        if self.gadgetRegex['favFilters'].matcher(dashPage).find():
            gadgetsReq.GET(extract(dashPage, self.gadgetRegex['favFilters'], (('\/', '/'), )))
        ignoredReq.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        gadgetsReq.GET('/rest/gadget/1.0/favfilters?showCounts=true')
        gadgetsReq.GET('/rest/gadget/1.0/intro')
        activityReq.GET('/plugins/servlet/streams?maxResults=5&streams=')        
        ignoredReq.GET('/rest/activity-stream/1.0/preferences')        
        gadgetsReq.GET('/rest/gadget/1.0/issueTable/jql?jql=assignee+=+currentUser()+AND+resolution+=+unresolved+ORDER+BY+priority+DESC,+created+ASC&num=10&addDefault=true&columnNames=&enableSorting=true&sortBy=null&paging=true&startIndex=0&showActions=true')
        ignoredReq.GET('/s/en_USdtocgs/710/2/5.0.3/_/download/resources/com.atlassian.streams.streams-jira-plugin:date-en-US/date.js?callback=ActivityStreams.loadDateJs&_=1328243472509')

        if self.gadgetRegex['admin'].matcher(dashPage).find():
            gadgetsReq.GET('/rest/gadget/1.0/admin')
            gadgetsReq.GET(extract(dashPage, self.gadgetRegex['admin'], (('\/', '/'), )))
                
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/ea75faef00e48ef63a752fe32a0374a0/_/download/contextbatch/css/atl.dashboard/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/bd834725165a74ae1301b76c58782bbb/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/bd834725165a74ae1301b76c58782bbb/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/create_12.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/tools_20.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.dashboard:dashboard/css/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            cacheReq.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:content-retrievers/jira.webresources:content-retrievers.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:inline-layer/jira.webresources:inline-layer.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:list/jira.webresources:list.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:dropdown/jira.webresources:dropdown.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:issue-table/jira.webresources:issue-table.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:g-assigned-to-me/com.atlassian.jira.gadgets:g-assigned-to-me.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs/com.atlassian.auiplugin:ajs.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets/com.atlassian.gadgets.publisher:ajs-gadgets.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-default/com.atlassian.streams:date-default.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common/com.atlassian.jira.gadgets:common.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:date-js/com.atlassian.streams:date-js.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streams-parent-js/com.atlassian.streams:streams-parent-js.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:datepicker/com.atlassian.streams:datepicker.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams.actions:inlineActionsJs/com.atlassian.streams.actions:inlineActionsJs.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams.actions:commentActionHandlers/com.atlassian.streams.actions:commentActionHandlers.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams.jira.inlineactions:actionHandlers/com.atlassian.streams.jira.inlineactions:actionHandlers.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsGadgetResources/com.atlassian.streams:streamsGadgetResources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/batch/com.atlassian.streams:streamsWebResources/com.atlassian.streams:streamsWebResources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:g-admin/com.atlassian.jira.gadgets:g-admin.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/throbber.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/full-view-icon.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/feed-icon.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/gadget-loading.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.1.0/_/download/resources/com.atlassian.streams:streamsWebResources/images/list-view-icon.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/resources/com.atlassian.auiplugin:ajs-gadgets-base/images/icons/aui-icon-tools.gif')
            cacheReq.GET('/images/64jira.png')
            cacheReq.GET('/images/icons/bug.gif')
            cacheReq.GET('/images/icons/priority_major.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/icon_descending.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/menu_indicator_for_light_backgrounds.gif')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/sprites/icons_module.png')


    # go to home page, which redirects to dashboard
    def notLoggedIn(self, cached=False):
        ignoredReq = self.requests['ignored_req']
        gadgetsReq=self.requests['gadgets']
        cacheReq = self.requests['dashboard_cache']
        req = self.requests['home_not_logged_in']
        ignoredReq.GET('/')
        ignoredReq.GET('/secure/MyJiraHome.jspa')
        dashPage = req.GET('/secure/Dashboard.jspa').text

        ignoredReq.POST('/plugins/servlet/gadgets/dashboard-diagnostics', ( NVPair('uri', host()+'/secure/Dashboard.jspa'), ), ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ) )
        gadgetsReq.GET(extract(dashPage, self.gadgetRegex['intro'], (('\/', '/'), )))
        gadgetsReq.GET(extract(dashPage, self.gadgetRegex['login'], (('\/', '/'), )))
        gadgetsReq.GET('/rest/gadget/1.0/intro')
        
        if not cached:
            cacheReq.GET('/s/en_US-4qg4om/732/5/ea75faef00e48ef63a752fe32a0374a0/_/download/contextbatch/css/atl.dashboard/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/bd834725165a74ae1301b76c58782bbb/_/download/contextbatch/css/atl.general/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:global-static/jira.webresources:global-static.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/js/batch.js?cache=false')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:dashboard/com.atlassian.gadgets.dashboard:dashboard.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/bd834725165a74ae1301b76c58782bbb/_/download/contextbatch/js/atl.general/batch.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-core-resources/com.atlassian.gadgets.embedded:gadget-core-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.embedded:gadget-container-resources/com.atlassian.gadgets.embedded:gadget-container-resources.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1/_/download/superbatch/css/batch.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:calendar-en/jira.webresources:calendar-en.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.dashboard:gadget-dashboard-resources/com.atlassian.gadgets.dashboard:gadget-dashboard-resources.js')
            cacheReq.GET('/rest/api/1.0/shortcuts/732/a9956f6e419d5b60b3f108b18ed9a75f/shortcuts.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/_/images/jira111x30.png')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/images/icons/new/icon18-charlie.png')
            cacheReq.GET('/plugins/servlet/gadgets/js/auth-refresh.js?v=61901225c398ca5626e5f70307fcebd&container=atlassian&debug=0')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:jquery-compatibility/com.atlassian.auiplugin:jquery-compatibility.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:aui-core/com.atlassian.auiplugin:aui-core.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.5.5/_/download/batch/com.atlassian.auiplugin:ajs-gadgets-base/com.atlassian.auiplugin:ajs-gadgets-base.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:util/com.atlassian.gadgets.publisher:util.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajax/com.atlassian.gadgets.publisher:ajax.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:ajs-gadgets-lite/com.atlassian.gadgets.publisher:ajs-gadgets-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:util-lite/jira.webresources:util-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:common-lite/com.atlassian.jira.gadgets:common-lite.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/batch/com.atlassian.gadgets.publisher:templates/com.atlassian.gadgets.publisher:templates.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/1.0/_/download/batch/jira.webresources:captcha/jira.webresources:captcha.css')
            cacheReq.GET('/s/en_US-4qg4om/732/5/5.0.5-rc1/_/download/batch/com.atlassian.jira.gadgets:g-login/com.atlassian.jira.gadgets:g-login.js')
            cacheReq.GET('/s/en_US-4qg4om/732/5/3.1.21/_/download/resources/com.atlassian.gadgets.publisher:ajs-gadgets-lite/images/loading.gif')
            cacheReq.GET('/images/64jira.png')
