from env import randint, randChoice, pause, resume
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPlugin
from net.grinder.plugin.http import HTTPPluginControl
from config import Config
from dashboard import Dashboard
from issuelist import IssueList
from issuenav import IssueNavigator
from savedfilter import SavedFilter
from userprofile import UserProfile
from projectlist import ProjectList
from userlist import UserList
from issuecreator import IssueCreator
from reports import Reports
from useradmin import UserAdmin
from projectadmin import ProjectAdmin
from issuetypeadmin import IssueTypeAdmin
from workflowadmin import WorkflowAdmin
from screenadmin import ScreenAdmin
from notificationschemeadmin import NotificationSchemeAdmin
from priorityadmin import PriorityAdmin
from statusadmin import StatusAdmin
from resolutionadmin import ResolutionAdmin
from permissionschemeadmin import PermissionSchemeAdmin
from systeminfoadmin import SystemInfoAdmin
from loggingprofilingadmin import LoggingProfilingAdmin
from mailServerAdmin import MailServerAdmin
from generalconfigadmin import GeneralConfigAdmin
from websudo import WebSudo

l = grinder.logger
config = Config()
userList = UserList(config) 

dashboard = Dashboard(100)
issueNav = IssueNavigator(200)
savedFilter = SavedFilter(300)
userProfile = UserProfile(400)
projectList = ProjectList(500,  config.getProjectFile())
issueList = IssueList(600, config)
issueCreator = IssueCreator(700)
reports = Reports(800)

userAdmin = UserAdmin(900)
projectAdmin = ProjectAdmin(1000)
issueTypeAdmin = IssueTypeAdmin(1100)
workflowAdmin = WorkflowAdmin(1200)
screenAdmin = ScreenAdmin(1300)
notificationSchemeAdmin = NotificationSchemeAdmin(1400)
priorityAdmin = PriorityAdmin(1500)
statusAdmin = StatusAdmin(1600)
resolutionAdmin = ResolutionAdmin(1700)
permissionSchemeAdmin = PermissionSchemeAdmin(1800)
systemInfoAdmin = SystemInfoAdmin(1900)
loggingProfilingAdmin = LoggingProfilingAdmin(2000)
mailServerAdmin = MailServerAdmin(2100)
generalconfigadmin = GeneralConfigAdmin(2200)
websudo = WebSudo(2300)
thinkTime = config.getThinkTime()

logLevels = ['DEBUG', 'INFO', 'WARN', 'ERROR']
lowercase = 'abcdefghijklmnopqrstuvwxyz'
uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
issues = issueList.getIssues()
projects = projectList.getProjects()

def think():
     if thinkTime > 0:
         #context=HTTPPluginControl.getThreadHTTPClientContext().getThreadContext();
         #context.pauseClock()
        grinder.sleep(thinkTime)
        #context.resumeClock()


def goHome():
    log('DASHBOARD NOT LOGGED IN')
    think()
    dashboard.notLoggedIn()
    
def doLogin():
    log('LOGIN & DASHBOARD')
    global user
    user = userList.getRandomUser()
    l.info('logging in as ' + user['username'] + ' :: ' + user['password'])
    think()
    dashboard.login(user['username'], user['password'])
    think()
    dashboard.loggedIn()
    
def doLoginAsAdmin():
    log('LOGIN & DASHBOARD')
    global user
    user = userList.getAdminUser()
    l.info('logging in as ' + user['username'] + ' :: ' + user['password'])
    think()
    dashboard.login(user['username'], user['password'])
    think()
    dashboard.loggedIn()
    
def browseIssues(count):
    log('VIEW ISSUES')
 
    for i in range(count):
        think()
        issue = issues[i]
        cached = i > 0
        l.info('viewing issue ' + issue.getKey() + ' cached=' + str(cached))
        issue.view(cached)
        
def browseIssueTabs(count):
    log('VIEW ISSUE TABS')
    
    for i in range(count):
        think()
        issue = issues[i]
        cached = i > 0
        l.info('viewing issue tab ' + issue.getKey() + ' cached=' + str(cached))
        action = i % 4
            
        if action == 0:
            issue.viewTabAll(cached)
        elif action == 1:
            issue.viewTabActivityStream(cached)
        elif action == 2:
            issue.viewTabChangeHistory(cached)
        elif action == 3:
            issue.viewTabComments(cached)
    
def createIssues(count):
    log('CREATE ISSUES')
    issues[0].view(True)
    
    for i in range(count):
        think()
        projectId = '13401'
        l.info('creating issue for project ' + str(projectId))
        issue_key = issueCreator.create(projectId, { 'summary' : 'test' + str(i), 'reporter' : user['username'] }, i > 0)
        l.info(issue_key)
    
def editIssues(count):
    log('EDIT ISSUES')

    edited = 0
    for i in range(len(issues)-1):
        issue = issues[randint(0, len(issues)-1)]
        think()
        data = issue.view(True)
        if data['editable']:
            l.info('editing issue ' + issue.getKey())
            think()
            issue.edit(data['atl_token'], { 'description' : 'test ' + str(i) }, i>0)
            edited += 1
            if (edited >= count):
                break
    
def addComments(count):
    log('ADD COMMENTS')
    
    for i in range(count):
        think()
        issue = issues[randint(0, len(issues)-1)]
        l.info('commenting on issue ' + issue.getKey())
        token = issue.view(True)['atl_token']
        think()
        issue.addComment(token, { 'comment' : 'test ' + str(i) }, True)
    
def transitionIssues(count):
    log('WORKFLOW TRANSITION ON ISSUES')
    
    for index in range(count):
        issue = issues[index] 
        think()
        data = issue.view(True)
        actions = data['workflow_actions']
        nextStep = actions[randint(0, len(actions)-1)]
        token = data['atl_token']
        l.info('transitioning issue ' + issue.getKey() + ' to step ' + nextStep)
        think()
        issue.nextWorkflowStep(token, nextStep, {}, True)
    
def searchIssues(count):
    log('ISSUE SEARCH')
    think()
    issueNav.simpleSearch({ 'query' : '', 'summary' : 'true' })
    think()
    issueNav.simpleSearch({ 'query' : '', 'summary' : 'true' }, True)

    for i in range(count-3):
        searchType = i%7
        think()
        if searchType == 0:
            issueNav.advancedSearch("PROJECT = " + projectList.getRandomProject().getProjectKey(), i > 0)
        elif searchType == 1:
            issueNav.advancedSearch("PROJECT = " + projectList.getRandomProject().getProjectKey() + " AND CREATED > -5d", True)
        elif searchType == 2:
            issueNav.advancedSearch("PROJECT = " + projectList.getRandomProject().getProjectKey() + " AND assignee = currentUser()", True)
        elif searchType == 3:
            issueNav.advancedSearch("PROJECT = " + projectList.getRandomProject().getProjectKey() + " AND resolution is EMPTY", True)
        elif searchType == 4:
            issueNav.advancedSearch("summary ~ 'asparagus'", True)
        elif searchType == 5:
            issueNav.advancedSearch("comment ~ 'asparagus'", True)
        elif searchType == 6:
            issueNav.advancedSearch("reporter = currentUser()", True)

    issueNav.advancedSearch("assignee = currentUser() and resolution is empty", True)

    
def useSavedFilter(count):
    log('USE A SAVED FILTER')
    for i in range(count):
        filterId = 10000 + randint(0, 4)
        l.info('viewing saved filter ' + str(filterId))
        cached = i>0
        think()
        token = savedFilter.manage(cached)
        think()
        savedFilter.use(token, filterId, cached)

def browseProjects(count):
    log('BROWSE PROJECT TABS')
       
    for i in range(count):
        project = projects[randint(0, len(projects)-1)]
        l.info('viewing project ' + project.getProjectKey())
        action = i % 3
        cached = i > 0
        think()
        if action == 0:
            project.viewTabComponents(cached)
        elif action == 1:
            project.viewTabIssues(cached)
        elif action == 2:
            project.viewTabVersions(cached)
    
def browseUserProfile():
    log('USER PROFILE')
    think()
    userProfile.view(user['username'])

def viewDashboard(count):
    log('VIEW DASHBOARD')
    for i in range(count):
        think()
        dashboard.loggedIn(True)

def viewReports(count):
    log('VIEW REPORTS')
    for i in range(count):
        pause()
        projectId = projectList.getRandomProject().getProjectId()
        resume()
        think()
        cached = i>0
        if i%2 == 0:
            reports.viewCreatedVsResolved(projectId, cached)
        else:
            reports.viewRecentlyCreatedIssues(projectId, cached)

# ========================== ADMIN ==========================

def doWebsudoLogin():
    log('WEBSUDO SESSION')
    think()
    data = userAdmin.browseSudo()
    think()
    websudo.sudo(data['token'], data['destination'], user['password'])

def createUser(count):
    log('CREATE USERS')
    for i in range(count):
        name = randomLowercase(8)
        l.info('creating user ' + name)
        cached = i>0
        think()
        userAdmin.browse(cached)
        think()
        userAdmin.add({ 'username' : name, 'password' : name, 'email' : name + '@example.com' }, cached)

def createProject(count):
    log('CREATE PROJECT')
    for i in range(count):
        key = randomUppercase(6)
        cached = i>0
        l.info('creating project with key [' + key + ']')
        think()
        token = projectAdmin.browse(cached)
        think()
        projectAdmin.add(token, { 'name' : key, 'key' : key, 'lead' : user['username'] }, cached)

def editProject(count):
    log('EDIT PROJECT')
    for i in range(count):
        think()
        project = projectList.getRandomProject()
        cached = i>0
        l.info('editing project [' + project.getProjectKey() + ']')
        token = projectAdmin.browse(cached)
        think()
        projectAdmin.edit(token, {'id' : project.getProjectId(), 'key' : project.getProjectKey(), 'name' : 'edited'}, cached)

def createIssueType(count):
    log('CREATE ISSUE TYPE')
    for i in range(count):
        name =  randomLowercase(8)
        cached = i>0
        l.info('creating issue type ' + name)
        think()
        issueTypeAdmin.browse(cached)
        think()
        issueTypeAdmin.add({ 'name' : name }, cached)

def editIssueType(count):
    log('EDIT ISSUE TYPE')
    for i in range(count):
        cached = i>0
        think()
        issueTypeAdmin.browse(cached)
        think()
        issueTypeAdmin.edit({ 'id' : '1', 'name' : 'edited' }, cached)

def createWorkflow(count):
    log('CREATE WORKFLOW')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        workflowAdmin.browse(cached)
        think()
        workflowAdmin.add({'newWorkflowName' : name}, cached)

def editWorkflow(count):
    log('EDIT WORKFLOW')
    for i in range(count):
        cached = i>0
        think()
        token = workflowAdmin.browse(cached)
        think()
        workflowAdmin.edit(token, {'name' : 'Agile', 'description' : 'edited'}, cached)

def createScreen(count):
    log('CREATE SCREEN')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        screenAdmin.browse(cached)
        think()
        screenAdmin.add({ 'name' : name }, cached)

def editScreen(count):
    log('EDIT SCREEN')
    for i in range(count):
        cached = i>0
        think()
        token = screenAdmin.browse(cached)
        think()
        screenAdmin.edit(token, { 'id' : '1', 'description' : 'edited' }, cached)

def createNotificationScheme(count):
    log('CREATE NOTIFICATION SCHEME')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        token = notificationSchemeAdmin.browse(cached)
        think()
        notificationSchemeAdmin.add(token, { 'name' : name }, cached)

def editNotificationScheme(count):
    log('EDIT NOTIFICATION SCHEME')
    for i in range(count):
        cached = i>0
        think()
        token = notificationSchemeAdmin.browse(cached)
        think()        
        notificationSchemeAdmin.edit(token, { 'id' : '10000', 'description' : 'edited' }, cached)

def createPriority(count):
    log('CREATE PRIORITY')
    for i in range(count):
        cached = i>0
        name = randomLowercase(8)
        think()
        token = priorityAdmin.browse(cached)
        think()
        priorityAdmin.add(token, { 'name' : name }, cached)
    
def editPriority(count):
    log('EDIT PRIORITY')
    for i in range(count):
        cached = i>0
        think()
        priorityAdmin.browse(cached)
        think()
        priorityAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)

def createStatus(count):
    log('CREATE STATUS')
    for i in range(count):
        cached = i>0
        name = randomLowercase(8)
        think()
        token = statusAdmin.browse(cached)
        think()
        statusAdmin.add(token, { 'name' : name }, cached)
    
def editStatus(count):
    log('EDIT STATUS')
    for i in range(count):
        cached = i>0
        think()
        statusAdmin.browse(cached)
        think()
        statusAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)
 
def createResolution(count):
    log('CREATE RESOLUTION')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        token = resolutionAdmin.browse(cached)
        think()
        resolutionAdmin.add(token, { 'name' : name }, cached)

def editResolution(count):
    log('EDIT RESOLUTION')
    for i in range(count):
        cached = i>0
        think()
        resolutionAdmin.browse(cached)
        think()
        resolutionAdmin.edit({ 'id' : '1', 'description' : 'edited' }, cached)
 
def createPermissionScheme(count):
    log('CREATE PERMISSION SCHEME')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        permissionSchemeAdmin.browse(cached)
        think()
        permissionSchemeAdmin.add({ 'name' : name }, cached)
    
def editPermissionScheme(count):
    log('EDIT PERMISSION SCHEME')
    for i in range(count):
        cached = i>0
        think()
        permissionSchemeAdmin.browse(count)
        think()
        permissionSchemeAdmin.edit({ 'id' : '0', 'description' : 'edited' }, cached)
 
def browseSystemInfo(count=1):
    log('BROWSE SYSTEM INFO')
    for i in range(count):
        think()
        systemInfoAdmin.browse(i>0)
 
def changeLogLevel(count=1):
    log('CHANGE LOG LEVEL')
    for i in range(count):
        level = randChoice(logLevels)
        cached = i>0
        think()
        token = loggingProfilingAdmin.browse(cached)
        l.info('changing log level of [com.atlassian.jira.util.BugzillaImportBean] to ' + level)
        # change something that potentially doesn't cause huge amounts of logging, as not to skew the tests
        think()
        loggingProfilingAdmin.changeLogLevel(token, 'com.atlassian.jira.util.BugzillaImportBean', level, cached)
 
def addMailServer(count=1):
    log('ADD MAIL SERVER')
    for i in range(count):
        name = randomLowercase(8)
        cached = i>0
        think()
        mailServerAdmin.browse(cached)
        think()
        mailServerAdmin.addSmtpServer({ 'name' : name, 'from' : 'test@atlassian.com', 'servername' : 'localhost' }, cached)
 
def changeGeneralConfiguration(count=1):
    log('CHANGE INTRODUCTION')
    for i in range(count):
        cached = i>0
        intro = randomLowercase(8)
        think()
        generalconfigadmin.browse(cached)
        think()
        generalconfigadmin.edit({ 'introduction' : intro, 'useGzip' : 'false' }, cached)
    
# ========================== UTIL ==========================

def log(message):
    l.info('============================= ' + message + ' =============================')

def randomUppercase(length):
    text = ''
    for i in range(length):
        text += randChoice(uppercase)
    return text

def randomLowercase(length):
    text = ''
    for i in range(length):
        text += randChoice(lowercase)
    return text
