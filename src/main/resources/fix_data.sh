#!/bin/bash


HOST=$1
shift

DB=$1
shift

USERNAME=$1
shift

PASSWORD=$1
shift

TARGET_FOLDER=$1
shift


PASSWORD_RESET_SQL="UPDATE cwd_user SET credential = (SELECT credential FROM (SELECT * FROM cwd_user) AS x WHERE user_name = 'admin');"
USER_SELECT_SQL="SELECT child_name FROM cwd_membership WHERE parent_name = 'jira-developers'"

#JIRA_PASSWORD=$( mysql -B -e"SELECT credential FROM cwd_user WHERE user_name = 'admin'" -u$USERNAME -p$PASSWORD -h$HOST $DB | sed '1d') 
JIRA_PASSWORD="admin" 

echo "Setting all user passwords to that of the admin user" 

mysql -e"$PASSWORD_RESET_SQL"  -u$USERNAME -p$PASSWORD -h$HOST $DB


# Generate Grinder script data
echo "Creating a list of issue keys for grinder" 

mysql -B -e'SELECT pkey, id  FROM  jiraissue' -u$USERNAME -p$PASSWORD -h$HOST $DB | awk '{print $1","$2'} | sed '1d' > ${TARGET_FOLDER}/jira_issues.txt

echo "Creating a list of the projects for grinder" 

mysql -B -e'SELECT pname, id, pkey project FROM project' -u$USERNAME -p$PASSWORD -h$HOST $DB | awk 'BEGIN { FS = "\t" } {print $1","$2","$3'} | sed '1d' > ${TARGET_FOLDER}/jira_projects.txt

echo "Creating a list of the users for grinder" 

mysql -B -e"$USER_SELECT_SQL" -u$USERNAME -p$PASSWORD -h$HOST $DB | sed -e '1d' -e 's/$/,'$JIRA_PASSWORD'/g' > ${TARGET_FOLDER}/jira_users.txt
  
