#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    printf "usage: $0 <file to convert>\n"
    exit 1
fi

grep -a "^Test " $1 | awk '
BEGIN {
    FS="[ \"]+";
    timeInMiliSeconds='$(date +%s%3N)';
}

{
    desc = $14;
    for (i = 15; i < NF; i++)
        desc = desc " " $i;
    printf "default||na||0||%s||%.f||%.f||\n", desc, timeInMiliSeconds, $5 * 1000000;
}
'
