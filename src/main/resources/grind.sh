#!/bin/bash

GRINDER_AGENT_NUM=$1
shift

GRINDER_SCRIPT=$1
shift

GRINDERS=$1
shift

JIRA_URL=$1
shift

PERSONA=$1
shift

GRINDER_LOG_DIR=$1
shift


WORKING_FOLDER=$PWD

GRINDER_JAR=$WORKING_FOLDER/lib/grinder.jar
GRINDER_CLASS=net.grinder.Grinder

LOG_DIR=$WORKING_FOLDER/$GRINDER_LOG_DIR/${PERSONA}_$GRINDER_AGENT_NUM
AGENT_LOG=$LOG_DIR/agent_localhost.log

JVM_GC_ARGS="-XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xmx256m -XX:+UseParallelOldGC"
GRINDER_WORKER_ARGS="-Dagent.jira.host=$JIRA_URL -Dgrinder.jira.username=admin -Dgrinder.jira.password=admin -Dgrinder.jira.workers.total=$GRINDERS -Dgrinder.jira.workers.current=$GRINDER_AGENT_NUM $JVM_GC_ARGS"

mkdir $LOG_DIR

cd test_scripts

export CLASSPATH=$GRINDER_JAR
java -Dgrinder.jvm.arguments="$GRINDER_WORKER_ARGS" $JVM_GC_ARGS \
	-Dgrinder.logDirectory=$LOG_DIR \
	-Dgrinder.script=$GRINDER_SCRIPT -Dgrinder.useConsole=false \
	$GRINDER_CLASS > $AGENT_LOG 2>&1

